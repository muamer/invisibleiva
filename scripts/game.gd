extends Node

const GROUP_PLAYERS = "players"
const GROUP_ENEMIES = "enemies"
const GROUP_POINTS  = "points"

onready var score = 0 setget _set_score
onready var high_score = 0 setget _set_high_score

signal score_changed
signal high_score_changed

func _ready():
	stage_manager.connect("stage_changed", self, "_on_stage_changed")	
	pass

func _set_score(new_value):
	score = new_value
	
	if score > high_score:
		self.high_score = score
	emit_signal("score_changed")
	pass

func _set_high_score(new_value):
	high_score = new_value
	emit_signal("high_score_changed")
	pass

func _on_stage_changed():
	score = 0
	pass
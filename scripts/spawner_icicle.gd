extends Node2D

const scn_icicle = preload("res://scenes/icicle.tscn")
const AMOUNT_TO_FILL_VIEW = 3
const ICICLE_WIDTH = 161.5

func _ready():
	var iva = utils.get_main_node().get_node("iva")
	if iva:
		iva.connect("state_changed", self, "_on_iva_state_changed", [], CONNECT_ONESHOT)
	pass

func _on_iva_state_changed(iva):
	if iva.get_state() == iva.STATE_WALKING:
		start()
	pass

func start():
	go_init_pos()
	
	for i in range(AMOUNT_TO_FILL_VIEW):
		spawn_and_move()
	pass
	
func go_init_pos():
	randomize()
	
	var init_pos = Vector2()
	init_pos.x = get_viewport_rect().size.width + ICICLE_WIDTH/2
	
	var camera = utils.get_main_node().get_node("camera")
	
	# add offset of camera because iva is moving on hud screen
	if camera:
		init_pos.x += camera.get_total_pos().x
		
	set_pos(init_pos)
	pass

func spawn_and_move():
	spawn_icicle()
	go_next_position()
	pass

func spawn_icicle():
	var new_icicle = scn_icicle.instance()
	new_icicle.set_pos(get_pos())
	new_icicle.connect("exit_tree", self, "spawn_and_move")
	get_node("container").add_child(new_icicle)
	pass

func go_next_position():
	randomize()
	
	var next_pos = get_pos()
	var offset_x = rand_range(-60, 200)
	next_pos.x += ICICLE_WIDTH/2 + offset_x + ICICLE_WIDTH/2
	set_pos(next_pos)
	pass
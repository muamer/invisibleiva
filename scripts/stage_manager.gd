extends Node

const STAGE_GAME = "res://stages/game_stage.tscn"

signal stage_changed

func _ready():
	pass

func change_stage(stage_path):
	get_tree().change_scene(stage_path)
	emit_signal("stage_changed")
	pass
extends StaticBody2D

func _ready():
	randomize()
	add_to_group(game.GROUP_POINTS)
	var position = get_pos()
	position += Vector2(rand_range(-20, 30), 0)
	set_pos(position)
	pass
extends KinematicBody2D

onready var state = IdleState.new(self)
onready var velocity = Vector2(0, 0)
onready var visible = true
onready var colliding_enemy
onready var colliding_point

const STATE_IDLE = 0
const STATE_WALKING = 1
const STATE_HIT = 2

onready var time = 0
const GRAVITY = 200.0

signal state_changed

func _ready():
	set_process_input(true)
	set_fixed_process(true)
	add_to_group(game.GROUP_PLAYERS)
	get_node("area").connect("body_enter", self, "_on_body_enter")
	get_node("area").connect("body_exit", self, "_on_body_exit")
	pass

func _fixed_process(delta):
	state.update(delta)
	pass
	
func _input(event):
	state.input(event)
	pass

func _on_body_enter(other_body):
	if state.has_method("on_body_enter"):
		state.on_body_enter(other_body)
	pass

func _on_body_exit(other_body):
	if state.has_method("on_body_exit"):
		state.on_body_exit(other_body)
	pass
	
func set_state(new_state):
	state.exit()
	
	if new_state == STATE_IDLE:
		state = IdleState.new(self)
	elif new_state == STATE_WALKING:
		state = WalkingState.new(self)
	elif new_state == STATE_HIT:
		state = HitState.new(self)
	emit_signal("state_changed", self)
	pass
	
func get_state():
	if state extends IdleState:
		return STATE_IDLE
	elif state extends WalkingState:
		return STATE_WALKING
	elif state extends HitState:
		return STATE_HIT
	pass

class IdleState:
	var iva
	
	func _init(iva):
		self.iva = iva
		iva.get_node("anim").play("idle")
		pass

	func update(delta):
		pass
	
	func input(event):
		pass
	
	func exit():
		iva.get_node("anim").stop()
		pass

class WalkingState:
	var iva
	
	func _init(iva):
		self.iva = iva
		iva.get_node("anim").play("walking")
		iva.get_node("anim").set_speed(2)
		pass
		
	func update(delta):
		if iva.visible == false:
			iva.set_opacity(0.2)
		else:
			iva.set_opacity(1)
			if iva.colliding_enemy != null:
				iva.set_state(iva.STATE_HIT)
				pass

			if iva.colliding_point != null:
				iva.colliding_point.queue_free()
				game.score += 1
		
		iva.time += delta
		var v = log(iva.time)/log(3)
		
		if v < 0: v = 0
		if v > 4: v = 4

		iva.velocity.x = v
		if iva.velocity.x > 4: iva.velocity.x = 4

		var motion = iva.velocity
		motion = iva.move(motion)
		
		var animation = iva.get_node("anim")
		animation.set_speed(animation.get_speed() + v*0.0001)
		
		if iva.is_colliding():
			var normal = iva.get_collision_normal()
			motion = normal.slide(motion)
			iva.velocity = normal.slide(iva.velocity)
			iva.move(motion)
		pass
	
	func input(event):
		if event.is_action_pressed("hide"):
			iva.visible = false
		elif event.is_action_released("hide"):
			iva.visible = true
		pass
	
	func on_body_enter(other_body):
		if other_body.is_in_group(game.GROUP_ENEMIES):
			iva.colliding_enemy = other_body
		if other_body.is_in_group(game.GROUP_POINTS):
			iva.colliding_point = other_body
		pass
	
	func on_body_exit(other_body):
		if other_body.is_in_group(game.GROUP_ENEMIES):
			iva.colliding_enemy = null
		if other_body.is_in_group(game.GROUP_POINTS):
			iva.colliding_point = null
		pass

	func exit():
		iva.get_node("anim").stop()
		pass

class HitState:
	var iva
	
	func _init(iva):
		self.iva = iva
		
	func update(delta):
		pass
	
	func input(event):
		pass
	
	func exit():
		pass